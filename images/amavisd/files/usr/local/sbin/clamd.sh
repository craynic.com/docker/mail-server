#!/usr/bin/env bash

set -Eeuo pipefail

# fix the permissions to the LIB folder
chown -R clamav.clamav -- /var/lib/clamav
chmod -R go-w -- /var/lib/clamav/

# update the virus database first
/usr/bin/freshclam || true

# launch the clamd in foreground
/usr/sbin/clamd --foreground
