# Mail Server - Postfix, Dovecot, Roundcube

This project consists of several docker images for Postfix, Dovecot, Amavis,
Roundcube.

## Postfix

Postfix listens on the following ports:
* **25** (SMTP) with TLS enabled, SASL authentication disabled
* **587** (Submission) with TLS mandatory and 465 (SMTPS), SASL authentication mandatory
* **20025** with TLS mandatory for privileged systems to relay e-mails;
        this is _de facto_ an open relay, so it must be protected with a firewall

E-mails coming over port **25** are:
* incoming, anonymous e-mails (SASL authentication is disabled on this port)
* subject to Greylisting
* checked for DKIM 

E-mails coming over ports **465**, **587** and **20025** are:
* considered as "ORIGINATING" e-mails
* are subject to DKIM signing

## Local e-mails

The following local e-mails are defined, and should be overriden to real existing e-mails:

* sqlgrey@mail.local
* virus-alert@mail.local
* spam-alert@mail.local