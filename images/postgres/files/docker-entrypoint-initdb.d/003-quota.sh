#!/usr/bin/env bash

set -Eeuo pipefail

export PGUSER="$POSTGRES_USER"

psql "${MAIL_MAIL_PG_DBNAME}" <<-EOSQL
  create view "quota"."domains" as
  select
      "id",
      "name",
      "space_used",
      round("space_used" / 1024 / 1024,0) as "space_used_mb",
      "space_quota",
      round("space_quota" / 1024 / 1024,0) as "space_quota_mb",
      round("space_used" / "space_quota" * 100,0) as "quota_percentage"
  from "mail"."domains";

  create view "quota"."mailboxes" as
  select
      "m"."id",
      "m"."mailbox",
      "m"."domain",
      "d"."name" as "domain_name",
      "m"."space_used",
      round("m"."space_used" / 1024 / 1024,0) as "space_used_mb",
      round(
          (
              "m"."space_used"
                  + greatest(
                      0,
                      least("m"."space_quota" - "m"."space_used","d"."space_quota" - "d"."space_used")
                      )
              ) / 1024 / 1024,
          0
          ) as "space_quota_mb",
      round(
          ("m"."space_used"
               / (
                   "m"."space_used"
                       + greatest(
                           0,
                           least("m"."space_quota" - "m"."space_used","d"."space_quota" - "d"."space_used")
                           )
                   ) * 100
              ),
          0
          ) as "quota_percentage"
  from "mail"."mailboxes" "m"
      inner join "mail"."domains" "d"
          on "m"."domain" = "d"."id"
  where "m"."space_quota" is not null;
EOSQL
