#!/usr/bin/env bash

set -Eeuo pipefail

# create mail dir and fix permissions
mkdir -p /var/mail
chown -R mail.mail /var/mail
chmod -R a-x,o-rwx,u+rwX,g+rwX /var/mail

function writeSSLCertConfig() {
  HOSTNAME="$1"

  if [ -z "$HOSTNAME" ]; then
    return
  fi

  echo "ssl_cert = </etc/letsencrypt/live/$HOSTNAME/fullchain.pem"
  echo "ssl_key = </etc/letsencrypt/live/$HOSTNAME/privkey.pem"
}

function writeSSLProtocolConfig() {
  HOSTNAME="$1"
  PROTOCOL="$2"

  if [ -z "$HOSTNAME" ]; then
    return
  fi

  echo "protocol $PROTOCOL {"
  writeSSLCertConfig "$HOSTNAME" | sed -e "s/^/  /"
  echo "}"
}

(
  if [ -n "$MAIL_POSTMASTER_ADDRESS" ]; then
    echo "postmaster_address = $MAIL_POSTMASTER_ADDRESS"
    echo ""
  fi

  writeSSLCertConfig "$MAIL_SSLHOSTNAME"
  echo ""

  writeSSLProtocolConfig "${MAIL_SSLHOSTNAME_IMAP:-$MAIL_SSLHOSTNAME}" imap
  echo ""

  writeSSLProtocolConfig "${MAIL_SSLHOSTNAME_POP3:-$MAIL_SSLHOSTNAME}" pop3
  echo ""

  writeSSLProtocolConfig "${MAIL_SSLHOSTNAME_SIEVE:-$MAIL_SSLHOSTNAME}" sieve
  echo ""
) > /etc/dovecot/local.conf

# set Postgres credentials
find /etc/dovecot -type f -name "*-sql.conf.ext" | while read -r file; do
  sed -i "/^connect\s*=/d" "$file"

  echo "connect = host=$MAIL_DOVECOT_PG_HOST dbname=$MAIL_DOVECOT_PG_DBNAME user=$MAIL_DOVECOT_PG_USER password=$MAIL_DOVECOT_PG_PASS" >> "$file"
done

# launch dovecot
dovecot -F
