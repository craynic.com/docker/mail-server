#!/usr/bin/env bash

set -Eeuo pipefail

# test whether Postgres is alive
echo "Testing whether PostgreSQL is alive."
/usr/bin/pg_isready -h "$MAIL_SQLGREY_PG_HOST" -U "$MAIL_SQLGREY_PG_USER" -q

# update config file
(
  echo "db_type = Pg"
  echo "db_name = $MAIL_SQLGREY_PG_DBNAME"
  echo "db_host = $MAIL_SQLGREY_PG_HOST"
  echo "db_port = default"
  echo "db_user = $MAIL_SQLGREY_PG_USER"
  echo "db_pass = $MAIL_SQLGREY_PG_PASS"
) >> /etc/sqlgrey/sqlgrey.conf

# define TRAP & launch SQLgrey
echo "Launching SQLgrey..."
trap 'kill -TERM $PID' TERM

/usr/sbin/sqlgrey &

PID=$!

wait $PID
