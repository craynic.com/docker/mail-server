#!/usr/bin/env bash

set -Eeuo pipefail

export PGUSER="$POSTGRES_USER"

psql "${MAIL_MAIL_PG_DBNAME}" <<-EOSQL
  create table "dovecot"."quota" (
      "username" varchar(201) not null,
      "bytes" bigint not null default 0,
      "messages" int not null default 0,
      primary key ("username")
  );

  create view "dovecot"."password_db" as
  select
      "m"."mailbox" as "username",
      "d"."name" as "domain",
      "m"."password" as "password",
      concat(
          '*:bytes=',
          (
              "m"."space_used"
              + greatest(
                  0,
                  case
                      when "m"."space_quota" is null
                          then "d"."space_quota" - "d"."space_used"
                      else least("m"."space_quota" - "m"."space_used","d"."space_quota" - "d"."space_used")
                  end
              )
          )::text
      ) as "userdb_quota_rule"
  from "mail"."mailboxes" "m"
      inner join "mail"."domains" "d"
          on "m"."domain" = "d"."id"
  where "d"."is_active" = true and "m"."is_active" = true and "d"."login_enabled" = true and "m"."login_enabled" = true;

  create view "dovecot"."user_db" as
  select
      "m"."mailbox" as "username",
      "d"."name" as "domain",
      concat(
          '*:bytes=',
          (
              "m"."space_used"
              + greatest(
                  0,
                  case
                      when "m"."space_quota" is null
                          then "d"."space_quota" - "d"."space_used"
                      else least("m"."space_quota" - "m"."space_used", "d"."space_quota" - "d"."space_used")
                  end
              )
          )::text
      ) as "quota_rule"
  from "mail"."mailboxes" "m"
      inner join "mail"."domains" "d"
          on "m"."domain" = "d"."id"
  where "d"."is_active" = true and "m"."is_active" = true and "d"."login_enabled" = true and "m"."login_enabled" = true;
EOSQL
