#!/usr/bin/env bash

set -Eeuo pipefail

# check that all processes are running
[[ $(supervisorctl status | grep -cv "RUNNING") == 0 ]] || exit 1

# check open ports
SS_OUT=$(ss -ltnH)
PORTS=( "$@" )
for port in "${PORTS[@]}"
do
      (echo "$SS_OUT" | grep -q ":$port ") || exit 1
done

# explicit exit code
exit 0
