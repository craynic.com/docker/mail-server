#!/usr/bin/env bash

set -Eeuo pipefail

# set the mailname
echo "$MAIL_MAILNAME" > /etc/mailname

# fix the structure & permissions to the LIB folder
mkdir -pm 0755 -- /var/lib/amavis/{db,tmp}/
chown -R amavis.amavis -- /var/lib/amavis/
chmod -R g-w,o-rwx -- /var/lib/amavis/

# launch Amavis
/usr/sbin/amavisd-new foreground
