#!/usr/bin/env bash

set -Eeuo pipefail

# shellcheck disable=SC2120
function genPassword() {
  LENGTH="${1:-32}"

  tr -dc "[:alnum:]" < /dev/urandom | head -c"$LENGTH"
}

function setPassword() {
  ENV_FILE="$1"
  VAR_NAME="$2"
  PASS="$3"

  if grep -q "^$VAR_NAME\s*=" -- "$ENV_FILE"; then
    sed -i "s/^$VAR_NAME\s*=.*$/$VAR_NAME=$PASS/g" -- "$ENV_FILE"
  else
    echo "$VAR_NAME=$PASS" >> "$ENV_FILE"
  fi
}

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
ENV_FILE=${1:-"$SCRIPT_DIR/../.env"}

cat -- "$ENV_FILE" | grep -oe "^[-_[:alnum:]]\+_PASS" | while read -r ENV_VAR_NAME; do
  setPassword "$ENV_FILE" "$ENV_VAR_NAME" "$(genPassword 32)"
done
