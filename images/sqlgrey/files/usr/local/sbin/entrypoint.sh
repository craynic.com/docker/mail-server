#!/usr/bin/env bash

set -Eeuo pipefail

# run supervisord
supervisord -n -c /etc/supervisor/supervisord.conf
