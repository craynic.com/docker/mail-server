###############################################################################
#
# This instance receives mail from hosting servers on the following interfaces:
#
#   * port 25 from allowed servers (controlled by external firewall),
#       TLS mandatory
#
# The e-mail is passed to Amavis and then to the main instance for delivery.
#
###############################################################################

# hostname & domain, will be overriden from ENV
myhostname = smtp.example.com
mydomain = $myhostname

# logging to stdout
maillog_file = /dev/stdout

# various settings
message_size_limit = 51200000
biff = no
mynetworks_style = subnet
compatibility_level = 2

# no local delivery
mydestination =
alias_maps =
alias_database =
mailbox_transport = error:5.1.1 Mailbox unavailable

# content filtering
content_filter = smtp-amavis:amavisd:10025

# limits
smtpd_recipient_limit = 50
smtpd_recipient_overshoot_limit = 51
smtpd_hard_error_limit = 20
smtpd_client_recipient_rate_limit = 50
smtpd_client_connection_rate_limit = 10
smtpd_client_message_rate_limit = 25
default_extra_recipient_limit = 50
duplicate_filter_limit = 50
default_destination_recipient_limit = 50
smtp_destination_recipient_limit = $default_destination_recipient_limit

# TLS for SMTP
smtp_tls_security_level = encrypt
smtp_tls_loglevel = 1

# TLS for SMTPD
smtpd_tls_security_level = encrypt
smtpd_tls_key_file = /etc/letsencrypt/live/$myhostname/privkey.pem
smtpd_tls_cert_file = /etc/letsencrypt/live/$myhostname/fullchain.pem
smtpd_tls_loglevel = 1

# SMTP settings
smtpd_helo_restrictions =
    permit_mynetworks,
    reject
smtpd_relay_restrictions =
    permit_mynetworks,
    reject_unauth_destination
smtpd_recipient_restrictions =
    reject_unauth_pipelining,
    reject_non_fqdn_recipient,
    permit
smtpd_sender_restrictions =
    reject_unauth_pipelining,
    reject_non_fqdn_sender,
    reject_unknown_sender_domain,
    permit
smtpd_data_restrictions =
    reject_unauth_pipelining,
    reject_multi_recipient_bounce,
    permit