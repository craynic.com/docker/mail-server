#!/usr/bin/env bash

set -Eeuo pipefail

# define the secrets
SECRETS_FILE="/etc/postsrsd.secret"

# first, the main secret for signing
if [ -z "$MAIL_SRS_DOMAIN" ]; then
  echo "You must define the domain used for SRS!" >/dev/stderr
  exit 1
fi

# first, the main secret for signing
if [ -z "$MAIL_SRS_SECRET" ]; then
  echo "You must define the secret key!" >/dev/stderr
  exit 1
fi

echo -e "$MAIL_SRS_SECRET" > "$SECRETS_FILE"
chown root.root -- "$SECRETS_FILE"
chmod 0750 -- "$SECRETS_FILE"

# now, all other possible secrets for verification
env | grep "^MAIL_SRS_SECRET_OTHER" | cut -d"=" -f1 | sort -n | while read -r SECRET_ENV_NAME; do
  SECRET_VALUE=$(printenv "$SECRET_ENV_NAME")
  if [ -n "$SECRET_VALUE" ]; then
    echo -e "$SECRET_VALUE" >> "$SECRETS_FILE"
  fi
done

# launch PostSRSd
/usr/sbin/postsrsd -l"0.0.0.0" -4 -d"$MAIL_SRS_DOMAIN" -a"$MAIL_SRS_SEPARATOR" -X"$MAIL_SRS_EXCLUDE_DOMAIN" -s"$SECRETS_FILE"
