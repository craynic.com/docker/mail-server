#!/usr/bin/env bash

set -Eeuo pipefail

export PGUSER="$POSTGRES_USER"

psql "${MAIL_MAIL_PG_DBNAME}" <<-EOSQL

create table "mail"."domains" (
    "id" uuid not null default uuid_generate_v4(),
    "name" varchar(100) not null,
    "space_quota" bigint not null,
    "space_used" bigint not null default 0,
    "is_active" boolean not null default true,
    "login_enabled" boolean not null default true,
    primary key ("id"),
    unique ("name")
);

create table "mail"."dkim_keys" (
    "domain" uuid references "mail"."domains"("id"),
    "dkim_key" text not null,
    "dkim_selector" varchar(100) not null,
    "is_active" boolean not null default true,
    primary key ("domain")
);

create table "mail"."mailboxes" (
    "id" uuid not null default uuid_generate_v4(),
    "mailbox" varchar(100) not null,
    "domain" uuid not null references "mail"."domains"("id"),
    "password" varchar(1000) not null,
    "space_quota" bigint null default null,
    "space_used" bigint not null default 0,
    "is_catchall" boolean not null default false,
    "is_active" boolean not null default true,
    "login_enabled" boolean not null default true,
    "greylisting_enabled" boolean not null default true,
    primary key ("id"),
    unique ("domain", "mailbox")
);

create table "mail"."aliases" (
    "id" uuid not null default uuid_generate_v4(),
    "domain" uuid not null references "mail"."domains"("id"),
    "alias" varchar(100) not null,
    "target" varchar(1000) not null,
    "is_active" boolean not null,
    "greylisting_enabled" boolean not null,
    primary key ("id"),
    unique ("domain", "alias")
);

create table "mail"."domain_aliases" (
    "id" uuid not null default uuid_generate_v4(),
    "domain" uuid references "mail"."domains"("id"),
    "alias" varchar(100) not null,
    "is_active" boolean not null,
    primary key ("id"),
    unique ("domain", "alias")
);

create table "mail"."extra_senders" (
    "id" uuid not null default uuid_generate_v4(),
    "mailbox" uuid not null references "mail"."mailboxes"("id"),
    "sender" varchar(255) not null,
    "is_active" boolean not null default true,
    primary key ("id"),
    unique ("mailbox", "sender")
);

create table "mail"."sender_bcc" (
    "id" uuid not null default uuid_generate_v4(),
    "mailbox" uuid not null references "mail"."mailboxes"("id"),
    "bcc" varchar(255) not null,
    "is_active" boolean not null default true,
    primary key ("id"),
    unique ("mailbox", "bcc")
);

create table "mail"."spf_safe_domains" (
    "id" uuid not null default uuid_generate_v4(),
    "domain" varchar(100) not null,
    "is_active" boolean not null,
    primary key ("id"),
    unique ("domain")
);

EOSQL
