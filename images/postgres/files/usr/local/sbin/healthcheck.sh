#!/usr/bin/env bash

set -Eeuo pipefail

pg_isready -U postgres
