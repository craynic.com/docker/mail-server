#!/usr/bin/env bash

set -Eeuo pipefail

# configure the hostname
if [ -z "$MAIL_HOSTNAME" ]; then
  echo "Please define the MAIL_HOSTNAME environment variable." >/dev/stderr
  exit 1
fi

postconf -e myhostname="$MAIL_HOSTNAME"

# fix postfix permissions
postfix set-permissions

# set Postgres credentials
find /etc/postfix -type f -name "pgsql_*" | while read -r file; do
  sed -i "/^\(user\|password\|hosts\|dbname\)\s*=/d" "$file"
  (
    echo ""
    echo "user = $MAIL_POSTFIX_PG_USER"
    echo "password = $MAIL_POSTFIX_PG_PASS"
    echo "hosts = $MAIL_POSTFIX_PG_HOST"
    echo "dbname = $MAIL_POSTFIX_PG_DBNAME"
  ) >> "$file"
done

# launch postfix
postfix start-fg
