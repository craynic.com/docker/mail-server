#!/usr/bin/env bash

set -Eeuo pipefail

setPostgresUserPassword() {
  USERNAME="$1"
  PASSWORD="$2"

  echo "Changing password for user \"$USERNAME\""
  docker-compose exec -dT postgres psql -U postgres -c "ALTER USER \"$USERNAME\" WITH PASSWORD '$PASSWORD';"
}

getEnvValue() {
  ENV_FILE="$1"
  VARIABLE_NAME="$2"

  cat -- "$ENV_FILE" | grep -e "^$VARIABLE_NAME\s*=" | sed "s/^$VARIABLE_NAME\s*=\s*\(.*\)\s*$/\1/" | head -n 1
}

# The docker-compose's filtering is broken so we have to use docker's filtering
# Also, docker-compose cannot print container ID and service name in a format easy-to-parse
# so... we get the services from docker-compose, let them translate into container ID...
# ...then we use docker to filter out containers not DB-powered and not running (default filtering)
# and then we return the list of service names
getDbPoweredServices() {
  # get all the services
  docker-compose ps --service | while read -r SERVICE_NAME; do
    # translate them into service
    docker-compose ps -q "$SERVICE_NAME" | while read -r CONTAINER_ID; do
      if [[ $(docker ps -f "id=$CONTAINER_ID" -f "label=cz.realm.mail.db-powered" -q | wc -l) -ne 0 ]]; then
        echo "$SERVICE_NAME"
      fi
    done
  done
}

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
ENV_FILE="$SCRIPT_DIR/../.env"

# Check if Postgres is running
docker-compose exec postgres pg_isready -q || ( echo "Postgres must be running and accepting connections."; exit 1 )

# Generate new passwords
echo "Postgres is running, generating new passwords..."
"$SCRIPT_DIR"/genDbPasswords.sh "$ENV_FILE"

cat -- "$ENV_FILE" | grep -oe "^[-_[:alnum:]]\+_PASS" | while read -r PASS_ENV_VAR_NAME; do
  USER_ENV_VAR_NAME="${PASS_ENV_VAR_NAME%_PASS}_USER"

  setPostgresUserPassword \
    "$(getEnvValue "$ENV_FILE" "$USER_ENV_VAR_NAME")" \
    "$(getEnvValue "$ENV_FILE" "$PASS_ENV_VAR_NAME")"
done

# Restart services
echo "Looking for services needing restart..."
DB_POWERED_SERVICES=$(getDbPoweredServices)

if [[ -n "$DB_POWERED_SERVICES" ]]; then
  echo "Recreating DB-powered services"
  echo "$DB_POWERED_SERVICES" | cut -d":" -f1 | xargs docker-compose up --detach
else
  echo "No services need to be restarted"
fi
