#!/usr/bin/env bash

set -Eeuo pipefail

export PGUSER="$POSTGRES_USER"

psql "${MAIL_MAIL_PG_DBNAME}" <<-EOSQL
  create view "postfix"."virtual_alias_domains" as
  select
      "a"."alias" as "domain",
      "d"."name" as "target"
  from "mail"."domain_aliases" as "a"
       inner join "mail"."domains" as "d"
                  on "a"."domain"="d"."id"
  where
          "a"."is_active" = true
    and "d"."is_active" = true;

  create view "postfix"."virtual_alias_maps" as
      -- domain aliases
      (
          select
                 concat('@', "a"."alias") as "email",
                 concat('@', "d"."name")  as "alias"
          from "mail"."domain_aliases" as "a"
              inner join "mail"."domains" as "d"
                  on "a"."domain" = "d"."id"
          where
                "a"."is_active" = true
            and "d"."is_active" = true
          )
      union
      -- mail aliases
      (
          select
                 concat("m"."alias",'@',"d"."name") as "email",
                 case
                     when position('@' in "m"."target") > 0
                         then "m"."target"
                     else concat("m"."target",'@',"d"."name")
                 end as "alias"
          from "mail"."aliases" "m"
              inner join "mail"."domains" "d"
                  on "m"."domain" = "d"."id"
          where "d"."is_active" = true and "m"."is_active" = true
          )
      union
      -- catch all addresses
      (
          select
                 concat('@',"d"."name") as "email",
                 concat("m"."mailbox",'@',"d"."name") as "alias"
          from "mail"."mailboxes" "m"
              inner join "mail"."domains" "d"
                  on "m"."domain" = "d"."id"
          where "d"."is_active" = true and "m"."is_active" = true and "m"."is_catchall" = true
          )
      union
      -- mailboxes to itself (explicit confirmation of mailbox) when a domain has catchall address (is this necessary?)
      (
          select
                 concat("m"."mailbox",'@',"d"."name") as "email",
                 concat("m"."mailbox",'@',"d"."name") as "alias"
          from "mail"."mailboxes" as "m"
              inner join "mail"."domains" "d"
                  on "m"."domain" = "d"."id"
          where
                "d"."is_active" = true
            and "m"."is_active" = true
            and exists (
                select 1
                from "mail"."mailboxes" as "m1"
                where
                      "m1"."domain" = "d"."id"
                  and "m1"."is_catchall" = true
                  and "m1"."is_active" = true
                )
          );

  create view "postfix"."virtual_mailbox_domains" as
  select "name"
  from "mail"."domains"
  where "is_active" = true;

  create view "postfix"."virtual_mailbox_maps" as
  select
      "m"."mailbox" as "mailbox",
      "d"."name" as "domain"
  from "mail"."mailboxes" as "m"
      inner join "mail"."domains" "d"
          on "m"."domain" = "d"."id"
  where "d"."is_active" = true and "m"."is_active" = true;

  create view "postfix"."relay_domains" as
      (
          select "a"."alias" as "domain"
          from "mail"."domain_aliases" as "a"
              inner join "mail"."domains" as "d"
                  on "a"."domain" = "d"."id"
          where "a"."is_active" = true and "d"."is_active" = true
          )
      union
      (
          select "name"
          from "mail"."domains"
          where "is_active" = true
          );

  create view "postfix"."skip_srs_domains" as
      (
          select "a"."alias" as "domain"
          from "mail"."domain_aliases" as "a"
              inner join "mail"."domains" as "d"
                  on "a"."domain" = "d"."id"
          where "a"."is_active" = true and "d"."is_active" = true
          )
      union
      (
          select "name"
          from "mail"."domains"
          where "is_active" = true
          )
      union
      (
          select "domain"
          from "mail"."spf_safe_domains"
          where "is_active" = true
          );

  create view "postfix"."relay_maps" as
      -- active mailboxes
      (
          select
              concat("m"."mailbox",'@',"d"."name") as "email"
          from "mail"."mailboxes" as "m"
              inner join "mail"."domains" as "d"
                  on "m"."domain"="d"."id"
          where "m"."is_active" = true and "d"."is_active" = true
          )
      -- active aliases
      union
      (
          select
              concat("a"."alias",'@',"d"."name") as "email"
          from "mail"."aliases" as "a"
              inner join "mail"."domains" as "d"
                  on "a"."domain"="d"."id"
          where "a"."is_active" = true and "d"."is_active" = true
          group by "a"."alias", "d"."id"
          )
      -- catchall mailboxes
      union
      (
          select
              concat('@',"d"."name") as "email"
          from "mail"."mailboxes" as "m"
              inner join "mail"."domains" as "d"
                  on "m"."domain"="d"."id"
          where "m"."is_catchall" = true and "m"."is_active" = true and "d"."is_active" = true
          group by "d"."id"
          )
      -- active mailboxes on aliased domains
      union
      (
          select
              concat("m"."mailbox",'@',"a"."alias") as "email"
          from "mail"."mailboxes" as "m"
               inner join "mail"."domains" as "d"
                          on "m"."domain"="d"."id"
               inner join "mail"."domain_aliases" as "a"
                          on "a"."domain"="d"."id"
          where "m"."is_active" = true and "d"."is_active" = true and "a"."is_active" = true
          )
      -- active aliases on aliased domains
      union
      (
          select
              concat("a"."alias",'@',"da"."alias") as "email"
          from "mail"."aliases" as "a"
              inner join "mail"."domains" as "d"
                  on "a"."domain"="d"."id"
              inner join "mail"."domain_aliases" as "da"
                  on "da"."domain"="d"."id"
          where "a"."is_active" = true and "d"."is_active" = true and "da"."is_active" = true
          group by "a"."alias", "da"."id"
          )
      -- catchall mailboxes on aliased domains
      union
      (
          select
              concat('@', "da"."alias") as "email"
          from "mail"."mailboxes" as "m"
              inner join "mail"."domains" as "d"
                  on "m"."domain"="d"."id"
              inner join "mail"."domain_aliases" as "da"
                  on "da"."domain"="d"."id"
          where "m"."is_catchall" = true and "m"."is_active" = true and "d"."is_active" = true and "da"."is_active" = true
          group by "da"."id"
      );

  create view "postfix"."smtpd_sender_login_maps" as
      -- each mailbox can send as itself
      (
          select
              concat("m"."mailbox",'@',"d"."name") as "sender",
              concat("m"."mailbox",'@',"d"."name") as "sasl_login"
          from "mail"."mailboxes" as "m"
              inner join "mail"."domains" as "d"
                  on "m"."domain" = "d"."id"
          where "m"."is_active" = true and "d"."is_active" = true
      )
      union
      -- we can send on behalf of our aliases, if they directly translate to a mailbox
      (
          select
              concat("ma"."alias",'@',"md"."name") as "sender",
              concat("mm"."mailbox",'@',"md"."name") as "sasl_login"
          from "mail"."aliases" as "ma"
              inner join "mail"."domains" as "md"
                  on "ma"."domain"="md"."id"
              left join "mail"."mailboxes" as "mm"
                  on "ma"."target"="mm"."mailbox"
                      and "ma"."domain"="mm"."domain"
          where "ma"."is_active" = true and "mm"."is_active" = true and "md"."is_active" = true
      )
      union
      -- we can send on behalf of our domain aliases for all mailboxes
      (
          select
              concat("mm"."mailbox",'@',"mda"."alias") as "sender",
              concat("mm"."mailbox",'@',"md"."name") as "sasl_login"
          from "mail"."domain_aliases" as "mda"
              inner join "mail"."domains" as "md"
                  on "mda"."domain"="md"."id"
              inner join "mail"."mailboxes" as "mm"
                  on "mm"."domain"="md"."id"
          where "mm"."is_active" = true and "md"."is_active" = true and "mda"."is_active" = true
      )
      union
      -- we can send on behalf of all aliases cross domain aliases, if they directly translate to a mailbox
      (
          select
              concat("ma"."alias",'@',"mda"."alias") as "sender",
              concat("mm"."mailbox",'@',"md"."name") as "sasl_login"
          from "mail"."aliases" as "ma"
              inner join "mail"."domains" as "md"
                  on "ma"."domain"="md"."id"
              inner join "mail"."domain_aliases" as "mda"
                  on "mda"."domain"="md"."id"
              inner join "mail"."mailboxes" as "mm"
                  on "ma"."target"="mm"."mailbox" and "ma"."domain"="mm"."domain"
          where "ma"."is_active" = true and "mm"."is_active" = true and "md"."is_active" = true and "mda"."is_active" = true
      )
      union
      -- extra sender login maps
      (
          select
              "sm"."sender",
              concat("mm"."mailbox", '@', "md"."name") as "sasl_login"
          from "mail"."extra_senders" "sm"
              inner join "mail"."mailboxes" "mm"
                  on "sm"."mailbox"="mm"."id"
              inner join "mail"."domains" "md"
                  on "mm"."domain"="md"."id"
          where "sm"."is_active" = true and "mm"."is_active" = true and "md"."is_active" = true
      )
      union
      -- our postmaster can send on behalf of all domains (so that the domain is known in login maps)
      (
          select
              concat('@', "d"."name") as "sender",
              'postmaster@realm.cz' as "sasl_login"
          from "mail"."domains" as "d"
          where "d"."is_active" = true
      )
      union
      -- our postmaster can send on behalf of all domains (so that the domain is known in login maps)
      (
          select
              concat('@', "mda"."alias") as "sender",
              'postmaster@realm.cz' as "sasl_login"
          from "mail"."domain_aliases" as "mda"
              inner join "mail"."domains" as "md"
                  on "mda"."domain"="md"."id"
          where "mda"."is_active" = true and "md"."is_active" = true
      );

  create view "postfix"."sender_bcc_maps" as
  select
      "m"."mailbox",
      "d"."name" as "domain",
      concat("m"."mailbox",'@',"d"."name") as "sender",
      "b"."bcc"
  from "mail"."sender_bcc" as "b"
      inner join "mail"."mailboxes" as "m"
          on "m"."id"="b"."mailbox"
      inner join "mail"."domains" as "d"
          on "m"."domain"="d"."id"
  where "b"."is_active" = true and "m"."is_active" = true and "d"."is_active" = true;
EOSQL
