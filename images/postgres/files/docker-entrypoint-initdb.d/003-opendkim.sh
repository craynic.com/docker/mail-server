#!/usr/bin/env bash

set -Eeuo pipefail

export PGUSER="$POSTGRES_USER"

psql "${MAIL_MAIL_PG_DBNAME}" <<-EOSQL
  create view "opendkim"."key_table" as
  select
      "do"."name" as "domain",
      "dk"."dkim_selector" as "dkim_selector",
      "dk"."dkim_key" as "dkim_key"
  from "mail"."domains" "do"
      inner join "mail"."dkim_keys" "dk"
          on "do"."id" = "dk"."domain"
  where
        "do"."is_active" = true
    and "dk"."is_active" = true;

  create view "opendkim"."signing_table" as
  select
      "do"."name" as "domain"
  from "mail"."domains" "do"
      inner join "mail"."dkim_keys" "dk"
          on "do"."id" = "dk"."domain"
  where
        "do"."is_active" = true
    and "dk"."is_active" = true;
EOSQL
