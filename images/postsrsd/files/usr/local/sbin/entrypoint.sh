#!/usr/bin/env bash

set -Eeuo pipefail

# run supervisord
supervisord -n -c /etc/supervisord.conf
