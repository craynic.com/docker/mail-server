#!/usr/bin/env bash

set -Eeuo pipefail

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

NAME="$1"
CERTS_PATH=${2:-"$SCRIPT_DIR/../data/certs"}

if [ -z "$NAME" ]; then
  echo "Please provide FQDN name for the certificate."
  exit 1
fi

CERT_PATH="$CERTS_PATH/live/$NAME"
PRIV_KEY_PATH="$CERT_PATH/privkey.pem"
FULLCHAIN_PATH="$CERT_PATH/fullchain.pem"

# clear existing certificates
rm -rf -- "$CERT_PATH"

# create the target folder
mkdir -p -- "$CERT_PATH"

# generate the certificate
openssl req -x509 -nodes -subj "/CN=$NAME" -newkey rsa:4096 -keyout "$PRIV_KEY_PATH" -out "$FULLCHAIN_PATH" -days 3650
